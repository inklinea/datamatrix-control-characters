# Datamatrix Control Characters

# Slight modification of the Inkscape core extension Render>Barcode>Datamatrix
#
# https://gitlab.com/inkscape/extensions/-/blob/master/render_barcode_datamatrix.py
# Copyright (C) 2009 John Beard john.j.beard@gmail.co
#
# Modification by Inklinea 2022 - https://gitlab.com/inklinea
#
# This modification adds ascii control characters as shown on the codes page
# Render>Barcode>Datamatrix Control Characters
#
# For example : Inkscape&lt;LF&gt;is&lt;LF&gt;Cool
#
# Should give the following when scanned with a modern mobile phone camera:
#
# Inkscape
# Is
# Cool
